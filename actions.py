import subprocess as sp

import settings

site_path = settings.site_path

def hoot():
    sp.run("aplay ~/dotfiles/misc/hoot.wav", shell=True)

def display_message(msg):
    with open(site_path + 'index.html', 'w') as f:
        f.write(msg)
    sp.run('git add index.html', cwd=site_path, shell=True, capture_output=True)
    sp.run('git commit -m "hehe"', cwd=site_path, shell=True, capture_output=True)
    sp.run('git push origin master', cwd=site_path, shell=True, capture_output=True)
